const checkAccess = require('../db/checkAccess');
const UserData = require('../models/UserData');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            if (!data) return false;

            const date = req.body.date;
            const day = data.appointmentsUserTo.find(el => el.date === date);

            if (!day || !day.timeList.length) {
                res.send({
                    success: true,
                    data: [],
                });
            } else {
                const requests = day.timeList.map(appointment => {
                    return new Promise((resolve, reject) => {
                        UserData.findOne({ id: appointment.user }, (error, user) => {
                            if (error) {
                                reject(error);
                            }
                            resolve({
                                name: user.name,
                                phone: user.phone,
                                avatarSrc: user.avatarSrc,
                                time: appointment.time,
                                id: appointment.user,
                                date,
                            });
                        });
                    });
                });
                Promise.all(requests)
                    .then(value => {
                        res.send({
                            success: true,
                            data: value,
                        });
                    }, error => {
                        res.send({
                            success: false,
                            message: error,
                        });
                    });
            }
        });
};
