const checkAccess = require('../db/checkAccess');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            const { date } = req.body;
            const appointments = data.appointmentsToUser;

            if (!data) return false;

            if (appointments.length) {
                const currentDay = appointments.find(el => (date === el.date ? el : false));
                let timeList = currentDay ? currentDay.timeList : false;

                if (timeList) {
                    timeList = timeList.filter(el => !el.bookedBy);
                }

                if (timeList) {
                    res.send({
                        success: true,
                        data: timeList,
                    });
                } else {
                    res.send({
                        success: true,
                        data: [],
                    });
                }
            } else {
                res.send({
                    success: true,
                    data: [],
                });
            }
        });
};
