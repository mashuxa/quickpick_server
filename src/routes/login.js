const mongoose = require('mongoose');
const isFilledForm = require('../db/isFilledForm');
const User = require('../models/User');
const config = require('../db/config');
const UserData = require('../models/UserData');


module.exports = (req, res) => {
    const data = req.body;
    const db = mongoose.connection;

    if (!isFilledForm(data, res)) return;

    mongoose.connect(`${config.url}`, { useNewUrlParser: true });

    db.on('error', (error) => {
        res.send({
            success: false,
            message: `Error: 14. ${error} Please, try again or contact support`,
        });
    });

    db.once('open', () => {
        User.findOne({ email: data.email }, (error, user) => {
            if (error) {
                return res.send({
                    success: false,
                    message: `Error: 15.  ${error} Please, try again or contact support`,
                });
            }
            if (!user) {
                return res.send({
                    success: false,
                    message: 'Error: 16. You are not registered. Please, to register.',
                });
            }
            if (user.validatePassword(data.password)) {
                UserData.findOne(
                    { id: user._id }, (error, userData) => {
                        userData.countOfLogin = ++userData.countOfLogin;
                        userData.lastLoginDate = Date.now();
                        userData.save();
                    },
                );

                res.send({
                    success: true,
                    message: 'You are logged',
                    token: user.generateJWT(),
                    email: user.email,
                });
            } else {
                return res.send({
                    success: false,
                    message: 'Error: 17. Incorrect login or password.',
                });
            }
        });
    });
};
