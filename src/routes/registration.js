const mongoose = require('mongoose');
const isFilledForm = require('../db/isFilledForm');
const User = require('../models/User');
const UserData = require('../models/UserData');
const config = require('../db/config');


module.exports = (req, res) => {
    const userData = req.body;

    if (!isFilledForm(userData, res)) return;

    if (userData.password !== userData.repeatPassword) {
        res.send({
            success: false,
            message: 'Error: 6. Passwords are not equal',
        });
    } else if (!userData.agreements) {
        res.send({
            success: false,
            message: 'Error: 7. Agreements are not accepted',
        });
    } else {
        const db = mongoose.connection;

        mongoose.connect(`${config.url}`, { useNewUrlParser: true });
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', () => {
            User.findOne({ email: userData.email }, (error, result) => {
                if (error) {
                    console.log(error);
                    res.send({
                        success: false,
                        message: 'Error: 8. Failed to add user. Please, try again or contact with support',
                    });
                } else if (result) {
                    res.send({
                        success: false,
                        message: 'Error: 9. You are already registered',
                    });
                } else {
                    const user = new User();
                    const data = new UserData();

                    data.id = user._id;
                    user.email = userData.email;
                    user.setPassword(userData.password);
                    user.save((err) => {
                        if (err) {
                            res.send({
                                success: false,
                                message: 'Error: 10. Failed to add user. Please, try again or contact support',
                            });
                        } else {
                            data.save((er) => {
                                if (er) {
                                    res.send({
                                        success: false,
                                        message: 'Error: 11. Failed to add user. Please, try again or contact support',
                                    });
                                } else {
                                    res.send({
                                        success: true,
                                        message: 'You are registered successfully. Thank you for registration! Please, login to get full access to the application',
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });
    }
};
