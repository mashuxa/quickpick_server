const checkAccess = require('../db/checkAccess');
const UserData = require('../models/UserData');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            if (!data) return false;

            const date = req.body.date;

            UserData.findOne({
                id: data.id,
            }, (err, user) => {
                const day = user.appointmentsToUser.find(el => el.date === date);
                if (!day) {
                    res.send({
                        success: true,
                        data: [],
                    });
                } else {
                    const list = day.timeList.filter(appointment => appointment.bookedBy);

                    if (!list.length) {
                        return res.send({
                            success: true,
                            data: [],
                        });
                    }

                    const requests = list.map((appointment) => {
                        return new Promise((resolve, reject) => {
                            UserData.findOne({ id: appointment.bookedBy }, (error, user) => {
                                if (error) {
                                    reject(error);
                                }
                                resolve({
                                    name: user.name,
                                    phone: user.phone,
                                    avatarSrc: user.avatarSrc,
                                    time: appointment.time,
                                    id: appointment.bookedBy,
                                    date,
                                });
                            });
                        });
                    });

                    Promise.all(requests)
                        .then(value => {
                            res.send({
                                success: true,
                                data: value,
                            });
                        }, error => {
                            res.send({
                                success: false,
                                message: error,
                            });
                        });
                }
            });
        });
};
