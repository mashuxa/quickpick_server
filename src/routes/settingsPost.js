const checkAccess = require('../db/checkAccess');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            if (!data) return false;

            const updateData = req.body;
            const img = req.file;

            if (updateData) {
                data.name = updateData.name;
                data.phone = updateData.phone;
                data.description = updateData.description;

                if (img) {
                    data.avatarSrc = `/avatars/${img.filename}`;
                }

                data.save();
                res.send({
                    success: true,
                    data: {
                        name: data.name,
                        phone: data.phone,
                        description: data.description,
                        avatarSrc: data.avatarSrc,
                    },
                });
            } else {
                res.send({
                    success: false,
                    message: 'Some error with uploading',
                });
            }
        });
};
