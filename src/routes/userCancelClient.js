const checkAccess = require('../db/checkAccess');
const UserData = require('../models/UserData');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            if (!data) return false;

            const userData = req.body;
            const day = data.appointmentsToUser.find(el => el.date === userData.date);
            const appointmentIndex = day.timeList.findIndex(el => {
                return (el.bookedBy === userData.id && el.time === userData.time);
            });

            if (appointmentIndex !== -1) {
                day.timeList.splice(appointmentIndex, 1);
                data.markModified('appointmentsToUser');
                data.save();
            }

            UserData.findOne({ id: userData.id }, (error, user) => {
                if (error) return;

                const day = user.appointmentsUserTo.find(el => el.date === userData.date);
                const appointmentIndex = day.timeList.find(el => {
                    return el.bookedBy === data.id && el.time === userData.time;
                });

                if (appointmentIndex !== -1) {
                    day.timeList.splice(appointmentIndex, 1);
                    user.markModified('appointmentsUserTo');
                    user.save();
                }

                res.send({
                    success: true,
                });
            });
        });
};
