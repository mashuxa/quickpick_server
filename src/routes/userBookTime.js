const checkAccess = require('../db/checkAccess');
const UserData = require('../models/UserData');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            if (!data) return false;
            const userData = req.body;

            UserData.findOne({
                id: userData.id,
            }, (err, user) => {
                const day = user.appointmentsToUser.find((el) => {
                    return el.date === userData.date;
                });

                if (!day) {
                    res.send({
                        success: false,
                        message: 'This date is not available',
                    });
                } else {
                    const appointment = day.timeList.find((appointment) => {
                        return (appointment.time === userData.time) && !appointment.bookedBy;
                    });

                    if (appointment) {
                        appointment.bookedBy = data.id;

                        user.markModified('appointmentsToUser');
                        user.save();

                        const appointmentDay = data.appointmentsUserTo.find((day) => {
                            return day.date === userData.date;
                        });

                        if (!appointmentDay) {
                            data.appointmentsUserTo.push({
                                date: userData.date,
                                timeList: [
                                    {
                                        time: userData.time,
                                        user: user.id,
                                    },
                                ],
                            });
                        } else {
                            appointmentDay.timeList.push({
                                time: userData.time,
                                user: user.id,
                            });
                        }

                        data.markModified('appointmentsUserTo');
                        data.save();
                        res.send({
                            success: true,
                            message: 'You are listed',
                        });
                    } else {
                        res.send({
                            success: false,
                            message: 'This time is not available',
                        });
                    }
                }
            });
        });
};
