const checkAccess = require('../db/checkAccess');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            const updateData = req.body;
            const appointments = data.appointmentsToUser;

            if (!data) return false;

            if (appointments.length) {
                updateData.forEach((day) => {
                    const currentDay = appointments.find(el => (day.date === el.date ? el : false));

                    if (currentDay) {
                        currentDay.timeList.push(...day.timeList);
                    } else {
                        appointments.push(day);
                    }
                });
            } else {
                appointments.push(...updateData);
            }

            data.markModified('appointmentsToUser');
            data.save();
            res.send({
                success: true,
            });
        });
};
