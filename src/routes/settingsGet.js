const checkAccess = require('../db/checkAccess');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then(data => (data ? res.send({
            success: true,
            data: {
                name: data.name,
                phone: data.phone,
                description: data.description,
                avatarSrc: data.avatarSrc,
            },
        }) : false));
};
