const checkAccess = require('../db/checkAccess');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            const { date } = req.body;
            const index = req.body.timeIndex;
            const appointments = data.appointmentsToUser;

            if (!data) return false;

            if (appointments.length) {
                const currentDay = appointments.find(el => (date === el.date ? el : false));

                if (currentDay) {
                    currentDay.timeList.splice(index, 1);
                    data.markModified('appointmentsToUser');
                    data.save();
                    res.send({
                        success: true,
                        data: currentDay.timeList,
                    });
                } else {
                    res.send({
                        success: false,
                        msg: 'Error: 18. Times wasn\'t found',
                    });
                }
            } else {
                res.send({
                    success: false,
                    msg: 'Error: 19. Dates wasn\'t found',
                });
            }
        });
};
