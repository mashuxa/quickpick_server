const checkAccess = require('../db/checkAccess');
const UserData = require('../models/UserData');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            if (!data) return false;

            const userData = req.body;

            UserData.findOne({
                id: userData.id,
            }, (err, user) => {
                const day = user.appointmentsToUser.find(el => el.date === userData.date);

                if (!day) {
                    res.send({
                        success: true,
                        data: [],
                    });
                } else {
                    let timeList = day.timeList.filter(el => !el.bookedBy);

                    timeList = timeList.map(el => el.time);

                    res.send({
                        success: true,
                        data: timeList,
                    });
                }
            });
        });
};
