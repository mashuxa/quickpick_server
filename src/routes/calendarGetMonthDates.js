const checkAccess = require('../db/checkAccess');
const UserData = require('../models/UserData');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            if (!data) {
                return res.send({
                    success: false,
                    message: 'Error: 13',
                });
            }

            function findDates(appointments) {
                let date = req.body.date.split('-');

                date.pop();
                date = date.join('-');

                if (req.method === 'POST') {
                    if (appointments.length) {
                        let list = appointments.filter((el) => {
                            const isCurrentMonth = el.date.startsWith(date);
                            const isAnyOne = el.timeList.some(element => !element.bookedBy);

                            return isCurrentMonth && isAnyOne;
                        });

                        list = list.map(el => el.date.split('-')
                            .pop());

                        res.send({
                            success: true,
                            data: list,
                        });
                    } else {
                        res.send({
                            success: true,
                            data: [],
                        });
                    }
                }
            }

            if (req.body.id) {
                UserData.findOne({
                    id: req.body.id,
                }, (err, user) => {
                    findDates(user.appointmentsToUser);
                });
            } else {
                findDates(data.appointmentsToUser);
            }
        });
};
