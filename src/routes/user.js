const checkAccess = require('../db/checkAccess');
const UserData = require('../models/UserData');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {

            if (!data) return false;

            UserData.findOne({ id: req.params.id }, (err, user) => {
                if (err || !user) {
                    res.send({
                        success: false,
                        data: 'Error 20',
                    });
                } else {
                    res.send({
                        success: true,
                        data: {
                            id: user.id,
                            name: user.name,
                            phone: user.phone,
                            description: user.description,
                            avatarSrc: user.avatarSrc,
                        },
                    });
                }
            });
        });
};
