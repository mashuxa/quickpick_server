const checkAccess = require('../db/checkAccess');
const UserData = require('../models/UserData');

module.exports = (req, res) => {
    checkAccess(req, res)
        .then((data) => {
            if (!data) return false;

            UserData.find({}, (err, users) => {
                const startIndex = req.body.perPage * (req.body.page - 1);
                const lastIndex = startIndex + req.body.perPage;
                let result = users.map((user) => {
                    if (!user.isActive || user.id === data.id) return;

                    const pattern = req.body.textFilter.toLowerCase();
                    const startDate = new Date(req.body.startDate);
                    const endDate = new Date(req.body.endDate);

                    if (pattern !== '') {
                        const isInName = user.name && user.name.toLowerCase()
                            .indexOf(pattern) !== -1;
                        const isInDescription = user.description && user.description
                            .indexOf(pattern) !== -1;

                        if (!isInName && !isInDescription) return;
                    }

                    const date = user.appointmentsToUser.find((freeDate) => {
                        return freeDate.timeList.find((time) => {
                            if (!time.bookedBy) {
                                const userTime = new Date(`${freeDate.date}T${time.time}`);
                                return (userTime > startDate && userTime < endDate);
                            }
                        });
                    });

                    if (!date) {
                        return;
                    }

                    return {
                        id: user.id,
                        name: user.name,
                        phone: user.phone,
                        avatarSrc: user.avatarSrc,
                        description: user.description,
                    };
                });

                result = result.filter(element => element);

                res.send({
                    success: true,
                    data: result.slice(startIndex, lastIndex),
                    count: result.length,
                });
            });

        });
};
