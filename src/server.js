const express = require('express');
const cors = require('cors');
const handleRouter = require('./db/handleRouter');
const constants = require('./constants');


const app = express();

app.use(cors());
app.use(express.static('public'));

app.listen(constants.PORT, () => {
    console.log(`App's working and listening on port ${constants.PORT}`);
});

handleRouter(app);
