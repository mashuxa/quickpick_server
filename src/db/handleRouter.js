const bodyParser = require('body-parser');
const multer = require('multer');
const registration = require('../routes/registration');
const login = require('../routes/login');
const settingsGet = require('../routes/settingsGet');
const settingsPost = require('../routes/settingsPost');
const calendar = require('../routes/calendar');
const calendarGetList = require('../routes/calendarGetList');
const calendarDeleteTime = require('../routes/calendarDeleteTime');
const calendarGetMonthDates = require('../routes/calendarGetMonthDates');
const users = require('../routes/users');
const user = require('../routes/user');
const userGetList = require('../routes/userGetList');
const userBookTime = require('../routes/userBookTime');
const userGetClients = require('../routes/userGetClients');
const userGetAppointments = require('../routes/userGetAppointments');
const userCancelAppointment = require('../routes/userCancelAppointment');
const userCancelClient = require('../routes/userCancelClient');
const constants = require('../constants');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/avatars/');
    },
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}-${file.originalname}`);
    },
});
const upload = multer({ storage });


module.exports = (app) => {
    app.post(constants.PAGES.registration, bodyParser.json(), registration);

    app.post(constants.PAGES.login, bodyParser.json(), login);

    app.get(constants.PAGES.settings, bodyParser.json(), settingsGet);

    app.post(constants.PAGES.settings, upload.single('avatar'), settingsPost);

    app.post(constants.PAGES.calendar, bodyParser.json(), calendar);

    app.post(constants.PAGES.calendarGetList, bodyParser.json(), calendarGetList);

    app.post(constants.PAGES.calendarDeleteTime, bodyParser.json(), calendarDeleteTime);

    app.post(constants.PAGES.calendarGetMonthDates, bodyParser.json(), calendarGetMonthDates);

    app.post(constants.PAGES.users, bodyParser.json(), users);

    app.get(constants.PAGES.user, user);

    app.post(constants.PAGES.userGetList, bodyParser.json(), userGetList);

    app.post(constants.PAGES.userBookTime, bodyParser.json(), userBookTime);

    app.post(constants.PAGES.userGetClients, bodyParser.json(), userGetClients);

    app.post(constants.PAGES.userGetAppointments, bodyParser.json(), userGetAppointments);

    app.post(constants.PAGES.userCancelClient, bodyParser.json(), userCancelClient);

    app.post(constants.PAGES.userCancelAppointment, bodyParser.json(), userCancelAppointment);
};
