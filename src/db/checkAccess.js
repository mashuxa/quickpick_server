const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('./config');
const User = require('../models/User');
const UserData = require('../models/UserData');

module.exports = (req, res) => new Promise((resolve) => {
    const token = req.headers.authorization;
    const decodedJwt = jwt.decode(token);
    const { email } = decodedJwt;
    const db = mongoose.connection;

    mongoose.connect(`${config.url}`, { useNewUrlParser: true });

    db.on('error', () => res.send({
        success: false,
        message: 'Error: 1. Access denied',
    }));

    db.once('open', () => {
        User.findOne({ email }, (error, user) => {
            if (error) {
                return res.send({
                    success: false,
                    message: 'Error: 2. Access denied',
                });
            }

            if (!user) {
                return res.send({
                    success: false,
                    message: 'Error: 3. User not found',
                });
            }

            if (!user.jwtVerify(token)) {
                return res.send({
                    success: false,
                    message: 'Error: 4. Access denied',
                });
            }


            UserData.findOne({ id: user._id }, (err, data) => {
                if (err) {
                    return res.send({
                        success: false,
                        message: 'Error: 5. Access denied',
                    });
                }

                resolve(data);
            });
        });
    });
});
