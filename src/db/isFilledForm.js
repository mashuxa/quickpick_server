module.exports = (formData, res) => {
    const isFilled = Object.values(formData)
        .reduce((val, currentVal) => val && currentVal);

    if (isFilled) return true;

    res.send({
        success: false,
        message: 'Error: 12. Please, fill required fields',
    });

    return false;
};
