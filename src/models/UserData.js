const mongoose = require('mongoose');


const UserDataSchema = new mongoose.Schema({
    id: {
        type: String,
        required: true,
        unique: true,
    },
    name: String,
    phone: Number,
    address: Object,
    description: String,
    avatarSrc: String,
    schedule: Array,
    appointmentsToUser: Array,
    appointmentsUserTo: Array,
    tagsId: Array,
    isActive: {
        type: Boolean,
        default: true,
    },
    registrationDate: {
        type: Date,
        default: Date.now,
    },
    countOfLogin: {
        type: Number,
        default: 0,
    },
    lastLoginDate: {
        type: Date,
        default: Date.now,
    },
    countOfVisit: {
        type: Number,
        default: 0,
    },
    lastVisitDate: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model('UserData', UserDataSchema);
