const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../db/config');


const UserSchema = new mongoose.Schema({
    email: String,
    hash: String,
    salt: String,
});

UserSchema.methods.setPassword = function (password) {
    this.salt = bcrypt.genSaltSync(10);
    this.hash = bcrypt.hashSync(password, this.salt);
};

UserSchema.methods.validatePassword = function (password) {
    return bcrypt.compareSync(password, this.hash);
};

UserSchema.methods.generateJWT = function () {
    return jwt.sign({ email: this.email }, config.secret, { expiresIn: 600 * 600 });
};

UserSchema.methods.jwtVerify = function (token) {
    return jwt.verify(token, config.secret, error => !error);
};

module.exports = mongoose.model('User', UserSchema);
