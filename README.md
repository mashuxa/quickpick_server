# README #

QuickPick back-end API

### What is this repository for? ###

NodeJS web API for QuickPick service

### How do I get set up? ###

**just type** 
_npm i_

**For starting type**
_node --experimental-modules ./src/server.mjs_

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
